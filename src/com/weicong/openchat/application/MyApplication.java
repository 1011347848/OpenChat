package com.weicong.openchat.application;

import java.util.ArrayList;

import org.litepal.LitePalApplication;

import android.app.Activity;
import android.content.Context;

import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;

public class MyApplication extends LitePalApplication {
	
	private ArrayList<Activity> mActivitys = new ArrayList<Activity>();
	
	@Override
	public void onCreate() {
		super.onCreate();
		
		initImageLoader(getApplicationContext());
	}
	
	public void pushActivity(Activity activity) {
		mActivitys.add(activity);
	}
	
	public void clearActivitys() {
		for (Activity activity : mActivitys) {
			activity.finish();
		}
	}
	
	public void removeActivity(Activity activity) {
		mActivitys.remove(activity);
	}
	
	private void initImageLoader(Context context) {
		// This configuration tuning is custom. You can tune every option, you may tune some of them,
		// or you can create default configuration by
		//  ImageLoaderConfiguration.createDefault(this);
		// method.
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
				.threadPriority(Thread.NORM_PRIORITY - 2)
				.denyCacheImageMultipleSizesInMemory()
				.discCacheFileNameGenerator(new Md5FileNameGenerator())
				.tasksProcessingOrder(QueueProcessingType.LIFO)
				.writeDebugLogs() // Remove for release app
				.build();
		// Initialize ImageLoader with configuration.
		ImageLoader.getInstance().init(config);
	}
}
