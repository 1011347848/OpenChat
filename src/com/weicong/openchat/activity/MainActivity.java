package com.weicong.openchat.activity;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewConfiguration;
import android.view.Window;

import com.weicong.openchat.R;
import com.weicong.openchat.fragment.FindFragment;
import com.weicong.openchat.fragment.HistoryFragment;
import com.weicong.openchat.fragment.ProfileFragment;
import com.weicong.openchat.fragment.RosterListFragment;
import com.weicong.openchat.view.ChangeColorIconWithTextView;

public class MainActivity extends FragmentActivity implements OnClickListener, OnPageChangeListener {

	private ViewPager mViewPager;
	private List<Fragment> mTabs = new ArrayList<Fragment>();
	private FragmentStatePagerAdapter mAdapter;
	
	private List<ChangeColorIconWithTextView> mTabIndicator = new ArrayList<ChangeColorIconWithTextView>();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		//去掉操作栏上的图标
		getActionBar().setDisplayShowHomeEnabled(false);
		
		setContentView(R.layout.activity_main);
		
		//显示overflow图标
		setOverflowShowingAlways();
		
		mViewPager = (ViewPager) findViewById(R.id.viewPager);
		mViewPager.setOffscreenPageLimit(3);
		initTabs();
		mAdapter = new FragmentStatePagerAdapter(getSupportFragmentManager()) {

			@Override
			public int getCount() {
				return mTabs.size();
			}

			@Override
			public Fragment getItem(int arg0) {
				return mTabs.get(arg0);
			}
		};
		
		initTabIndicator();
		
		mViewPager.setAdapter(mAdapter);
		mViewPager.setOnPageChangeListener(this);
		
	}
	
	/**
	 * 初始化tab页面
	 */
	private void initTabs() {
		mTabs.add(new HistoryFragment());
		mTabs.add(new RosterListFragment());
		mTabs.add(new FindFragment());
		mTabs.add(new ProfileFragment());
	}
	
	/**
	 * 初始化tab标签
	 */
	private void initTabIndicator() {
		ChangeColorIconWithTextView one = (ChangeColorIconWithTextView) findViewById(R.id.tab_one);
		ChangeColorIconWithTextView two = (ChangeColorIconWithTextView) findViewById(R.id.tab_two);
		ChangeColorIconWithTextView three = (ChangeColorIconWithTextView) findViewById(R.id.tab_three);
		ChangeColorIconWithTextView four = (ChangeColorIconWithTextView) findViewById(R.id.tab_four);

		mTabIndicator.add(one);
		mTabIndicator.add(two);
		mTabIndicator.add(three);
		mTabIndicator.add(four);

		one.setOnClickListener(this);
		two.setOnClickListener(this);
		three.setOnClickListener(this);
		four.setOnClickListener(this);

		one.setIconAlpha(1.0f);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Intent intent = null;
		switch (item.getItemId()) {
		case R.id.action_add_friend:
			intent = new Intent(this, AddFriendActivity.class);
			startActivity(intent);
			return true;
		case R.id.action_scan:
			intent = new Intent(this, CaptureActivity.class);
			startActivity(intent);
			return true;
		}
		
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onClick(View v) {

		resetTabs();

		switch (v.getId()) {
		case R.id.tab_one:
			mTabIndicator.get(0).setIconAlpha(1.0f);
			mViewPager.setCurrentItem(0, false);
			break;
		case R.id.tab_two:
			mTabIndicator.get(1).setIconAlpha(1.0f);
			mViewPager.setCurrentItem(1, false);
			break;
		case R.id.tab_three:
			mTabIndicator.get(2).setIconAlpha(1.0f);
			mViewPager.setCurrentItem(2, false);
			break;
		case R.id.tab_four:
			mTabIndicator.get(3).setIconAlpha(1.0f);
			mViewPager.setCurrentItem(3, false);
			break;
		}
	}
	
	@Override
	public void onPageSelected(int arg0) {
		
	}

	@Override
	public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

		if (positionOffset > 0) {
			ChangeColorIconWithTextView left = mTabIndicator.get(position);
			ChangeColorIconWithTextView right = mTabIndicator.get(position + 1);

			left.setIconAlpha(1 - positionOffset);
			right.setIconAlpha(positionOffset);
		}

	}

	@Override
	public void onPageScrollStateChanged(int state) {

	}
	
	/**
	 * 恢复所有的tab标签
	 */
	private void resetTabs() {
		
		for (int i = 0; i < mTabIndicator.size(); i++) {
			mTabIndicator.get(i).setIconAlpha(0);
		}
	}
	
	@Override
	public boolean onMenuOpened(int featureId, Menu menu) {
		//让Overflow中的选项显示图标
		if (featureId == Window.FEATURE_ACTION_BAR && menu != null) {
			if (menu.getClass().getSimpleName().equals("MenuBuilder")) {
				try {
					Method m = menu.getClass().getDeclaredMethod("setOptionalIconsVisible", Boolean.TYPE);
					m.setAccessible(true);
					m.invoke(menu, true);
				} catch (Exception e) {
					
				}
			}
		}
		return super.onMenuOpened(featureId, menu);
	}
	
	/**
	 * 显示overflow图标
	 */
	private void setOverflowShowingAlways() {
		
		try {
			// true if a permanent menu key is present, false otherwise.
			ViewConfiguration config = ViewConfiguration.get(this);
			Field menuKeyField = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");
			menuKeyField.setAccessible(true);
			menuKeyField.setBoolean(config, false);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
