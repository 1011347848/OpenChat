package com.weicong.openchat.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.appkefu.lib.interfaces.KFIMInterfaces;
import com.appkefu.lib.service.KFSettingsManager;
import com.weicong.openchat.R;

public class ChangePasswordActivity extends Activity {

	private EditText mOldPassword;
	private EditText mNewPassword;
	private EditText mPasswordConfirm;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_change_password);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		setTitle("�޸�����");
		
		mOldPassword = (EditText) findViewById(R.id.et_password);
		mNewPassword = (EditText) findViewById(R.id.et_new_password);
		mPasswordConfirm = (EditText) findViewById(R.id.et_password_confirm);
		
		Button saveButton = (Button) findViewById(R.id.btn_save);
		saveButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				validate();
				hideSoftInputView();
			}
		});
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
		}
		
		return super.onOptionsItemSelected(item);
	}
	
	private void validate() {
		String oldPassword = mOldPassword.getText().toString();
		String newPassword = mNewPassword.getText().toString();
		String passwordConfirm = mPasswordConfirm.getText().toString();
		
		if (oldPassword.equals("")) {
			mOldPassword.setError("������ԭ����");
			return;
		}
		
		if (newPassword.equals("")) {
			mNewPassword.setError("������������");
			return;
		}
		
		if (passwordConfirm.equals("")) {
			mPasswordConfirm.setError("������ȷ������");
			return;
		}
		
		if (newPassword.length() < 6) {
			mNewPassword.setError("����6λ");
			return;
		}
		
		if (passwordConfirm.length() < 6) {
			mPasswordConfirm.setError("����6λ");
			return;
		}
		
		if (!oldPassword.equals(KFSettingsManager.getSettingsManager(this).getPassword())) {
			mOldPassword.setError("ԭ���벻��ȷ");
			return;
		}
		
		if (!newPassword.equals(passwordConfirm)) {
			mPasswordConfirm.setError("���벻һ��");
			return;
		}
		
		changePassword(newPassword);
	}
	
	private void changePassword(String password) {
		KFIMInterfaces.changePassword(password);
		finish();
	}
	
	/**
     * ����������
     */
    private void hideSoftInputView() {
		InputMethodManager manager = ((InputMethodManager) this.getSystemService(Activity.INPUT_METHOD_SERVICE));
		if (getWindow().getAttributes().softInputMode != WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN) {
			if (getCurrentFocus() != null)
				manager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
		}
	}
}
