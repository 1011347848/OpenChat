package com.weicong.openchat.activity;

import java.util.List;

import org.jivesoftware.smack.util.StringUtils;
import org.litepal.crud.DataSupport;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.text.ClipboardManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.appkefu.lib.interfaces.KFIMInterfaces;
import com.appkefu.lib.service.KFMainService;
import com.appkefu.lib.service.KFSettingsManager;
import com.appkefu.lib.utils.KFImageUtils;
import com.appkefu.lib.utils.KFUtils;
import com.appkefu.lib.xmpp.XmppVCard;
import com.weicong.openchat.R;
import com.weicong.openchat.adapter.ChatAdapter;
import com.weicong.openchat.entity.MessageEntity;
import com.weicong.openchat.face.FaceUtil;
import com.weicong.openchat.utils.ImageUtil;
import com.weicong.openchat.video.VideoRecordActivity;
import com.weicong.openchat.video.VideoUtil;
import com.weicong.openchat.voice.AudioRecordButton;
import com.weicong.openchat.voice.AudioRecordButton.AudioRecordFinishListener;
import com.weicong.openchat.voice.VoiceUtil;
import com.ypy.eventbus.EventBus;


@SuppressWarnings("deprecation")
public class ChatActivity extends Activity {
	
	private static final int REQUEST_VIDEO_RECORD = 111;
	private static final int REQUEST_IMAGE = 123;
	
	public static final String USERNAME = "username";
	public static final String FLAG = "flag";
	
	private String mUsername;
	
	private ListView mListView;
	
	private TextView mSendText;
	
	private EditText mInputText;
	
	private View mFaceContainer; //表情符号容器
	private View mPlusContainer; //更多功能容器
	
	private boolean mIsShowFace = false; //是否显示表情符号
	private boolean mIsShowPlus = false; //是否显示更多功能
	
	private Button mFaceButton; //表情按钮
	
	private Button mVoiceButton; //语音按钮
	
	private AudioRecordButton mRecordVoiceButton; //录制声音按钮
	
	private Button mPlusButton; //更多功能按钮
	
	private ChatAdapter mChatAdapter;
	private List<MessageEntity> mMessageList;
	
	private Bitmap mSendBitmap;
	private Bitmap mReceiveBitmap;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_chat);
		
		getActionBar().setDisplayHomeAsUpEnabled(true);
		mUsername = getIntent().getStringExtra(USERNAME);
		String nickname = KFIMInterfaces.getOtherNickname(mUsername);
		if (nickname == null) {
			nickname = mUsername;
		}
		setTitle(nickname);
		
		mFaceContainer = findViewById(R.id.chat_face_container);
		mPlusContainer = findViewById(R.id.chat_plus_container);
		
		mInputText = (EditText) findViewById(R.id.appkefu_inputbar_edittext);
		
		mInputText.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				hideFaceContainer();
				hidePlusContainer();
			}
		});
		
		mInputText.setOnFocusChangeListener(new OnFocusChangeListener() {
			
			@Override
			public void onFocusChange(View arg0, boolean arg1) {
				if (arg1) {
					hideFaceContainer();
					hidePlusContainer();
				}
			}
		});
		
		mInputText.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
				String s = arg0.toString();
				if (s.equals("")) {
					mSendText.setVisibility(View.GONE);
				} else {
					mSendText.setVisibility(View.VISIBLE);
				}
			}
			
			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				
			}
			
			@Override
			public void afterTextChanged(Editable arg0) {
				
			}
		});
		
		mFaceButton = (Button) findViewById(R.id.appkefu_inputbar_emotionbtn);
		mFaceButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				if (!mIsShowFace) {
					hideSoftInputView();
					hidePlusContainer();
					showFaceContainer();
				} else {
					hideFaceContainer();
				}
			}
		});
		
		mPlusButton = (Button) findViewById(R.id.appkefu_inputbar_plus);
		mPlusButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				if (!mIsShowPlus) {
					hideSoftInputView();
					hideFaceContainer();
					showPlusContainer();
				} else {
					hidePlusContainer();
				}
			}
		});
		
		mVoiceButton = (Button) findViewById(R.id.appkefu_inputbar_voice);
		mVoiceButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				if (mRecordVoiceButton.getVisibility() == View.GONE) {
					mVoiceButton.setBackgroundResource(R.drawable.appkefu_inputbar_keyboardbtn);
					mRecordVoiceButton.setVisibility(View.VISIBLE);
					hideSoftInputView();
					hideFaceContainer();
					hidePlusContainer();
					mInputText.setText("");
				} else {
					mVoiceButton.setBackgroundResource(R.drawable.appkefu_inputbar_voicebtn);
					mRecordVoiceButton.setVisibility(View.GONE);
				}
				
			}
		});
		
		mRecordVoiceButton = (AudioRecordButton) findViewById(R.id.appkefu_inputbar_recordvoicebtn);
		mRecordVoiceButton.setAudioRecordFinishListener(new AudioRecordFinishListener() {
			
			@Override
			public void onFinish(float time, String filePath) {
				String msg = VoiceUtil.getVoiceSendMsg(time, filePath);
				KFIMInterfaces.sendTextMessage(ChatActivity.this, msg, mUsername, "chat");
				
				msg = VoiceUtil.getVoiceMsg(time, filePath);
				MessageEntity entity = new MessageEntity();
				entity.setUsername(mUsername);
				entity.setContent(msg);
            	entity.setDate(KFUtils.getDate());
            	entity.setType(1);
            	entity.save(); //存储到数据库
            	
            	mMessageList.add(entity);
            	mChatAdapter.notifyDataSetChanged();
            	
            	EventBus.getDefault().post(entity);
			}
		});
		
		mListView = (ListView) findViewById(R.id.listView);
		registerForContextMenu(mListView);
		
		mSendText = (TextView) findViewById(R.id.tv_send);
		mSendText.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				String body = mInputText.getText().toString();
				mInputText.setText("");
				KFIMInterfaces.sendTextMessage(ChatActivity.this, body, mUsername, "chat");
				
				MessageEntity entity = new MessageEntity();
				entity.setUsername(mUsername);
				entity.setContent(body);
            	entity.setDate(KFUtils.getDate());
            	entity.setType(1);
            	entity.save(); //存储到数据库
            	
            	mMessageList.add(entity);
            	mChatAdapter.notifyDataSetChanged();
            	
            	EventBus.getDefault().post(entity);
			}
		});
		
		addFace();
		addPlus();
	}
	
	/**
	 * 增加表情功能
	 */
	private void addFace() {
		
		ViewPager viewPager = (ViewPager) findViewById(R.id.face_viewpager);
		LinearLayout layout = (LinearLayout) findViewById(R.id.face_dots_container);
		
		FaceUtil.init(this, viewPager, layout, 4, 6, mInputText);
	}
	
	/**
	 * 增加更多功能
	 */
	private void addPlus() {
		//视频按钮
		findViewById(R.id.btn_video).setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(ChatActivity.this, VideoRecordActivity.class);
				startActivityForResult(intent, REQUEST_VIDEO_RECORD);
			}
		});
		
		//图片按钮
		findViewById(R.id.btn_picture).setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(ChatActivity.this, SelectPictureActivity.class);
				intent.putExtra(FLAG, true);
				startActivityForResult(intent, REQUEST_IMAGE);
			}
		});
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == REQUEST_VIDEO_RECORD && resultCode == RESULT_OK) {
			String filePath = data.getStringExtra(VideoRecordActivity.FILE_PATH);
			
			sendVideoMsg(filePath);
		} else if (requestCode == REQUEST_IMAGE && resultCode == RESULT_OK) {
			String filePath = data.getStringExtra("path");
			sendImageMsg(filePath);
		}
	}
	
	/**
	 * 发送视频
	 * @param filePath
	 */
	private void sendVideoMsg(String filePath) {
		String msg = VideoUtil.getVideoSendMsg(filePath);
		
		KFIMInterfaces.sendTextMessage(ChatActivity.this, msg, mUsername, "chat");
		
		msg = VideoUtil.getVideoMsg(filePath);
		MessageEntity entity = new MessageEntity();
		entity.setUsername(mUsername);
		entity.setContent(msg);
    	entity.setDate(KFUtils.getDate());
    	entity.setType(1);
    	entity.save(); //存储到数据库
    	
    	mMessageList.add(entity);
    	mChatAdapter.notifyDataSetChanged();
    	
    	EventBus.getDefault().post(entity);
	}
	
	/**
	 * 发送图片
	 * @param filePath
	 */
	private void sendImageMsg(String filePath) {
		String msg = ImageUtil.getImageSendMsg(filePath);
		
		KFIMInterfaces.sendTextMessage(ChatActivity.this, msg, mUsername, "chat");
		
		msg = ImageUtil.getImageMsg(filePath);
		MessageEntity entity = new MessageEntity();
		entity.setUsername(mUsername);
		entity.setContent(msg);
    	entity.setDate(KFUtils.getDate());
    	entity.setType(1);
    	entity.save(); //存储到数据库
    	
    	mMessageList.add(entity);
    	mChatAdapter.notifyDataSetChanged();
    	
    	EventBus.getDefault().post(entity);
	}
	
	/**
	 * 隐藏表情面板
	 */
	private void hideFaceContainer() {
		mFaceButton.setBackgroundResource(R.drawable.appkefu_inputbar_emotionbtn);
		mFaceContainer.setVisibility(View.GONE);
		mIsShowFace = false;
	}
	
	/**
	 * 显示表情面板
	 */
	private void showFaceContainer() {
		mFaceButton.setBackgroundResource(R.drawable.appkefu_chatting_biaoqing_btn_enable);
		mFaceContainer.setVisibility(View.VISIBLE);
		mIsShowFace = true;
	}
	
	/**
	 * 隐藏更多功能按钮
	 */
	private void hidePlusContainer() {
		mPlusContainer.setVisibility(View.GONE);
		mIsShowPlus = false;
	}
	
	/**
	 * 显示更多功能按钮
	 */
	private void showPlusContainer() {
		if (mRecordVoiceButton.getVisibility() == View.VISIBLE) {
			mRecordVoiceButton.setVisibility(View.GONE);
			mVoiceButton.setBackgroundResource(R.drawable.appkefu_inputbar_voicebtn);
		}
		mPlusContainer.setVisibility(View.VISIBLE);
		mIsShowPlus = true;
	}
	
	/**
	 * 隐藏软键盘
	 */
	private void hideSoftInputView() { 
	     InputMethodManager manager = ((InputMethodManager)      this.getSystemService(Activity.INPUT_METHOD_SERVICE));
	     if (getWindow().getAttributes().softInputMode != WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN) {
	    	 if (getCurrentFocus() != null)
	    		 manager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),           
	    			 InputMethodManager.HIDE_NOT_ALWAYS);
	     }
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home) {
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		getMenuInflater().inflate(R.menu.chat_item_context_menu, menu);
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public boolean onContextItemSelected(MenuItem item) {
		AdapterContextMenuInfo info = (AdapterContextMenuInfo)item.getMenuInfo();
		int position = info.position;
		
		switch (item.getItemId()) {
		case R.id.action_copy:
			MessageEntity entity = mMessageList.get(position);
			ClipboardManager cmb = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
			cmb.setText(entity.getContent());
			return true;
		case R.id.action_delete:
			entity = mMessageList.remove(position);
			DataSupport.delete(MessageEntity.class, entity.getId());
			mChatAdapter.notifyDataSetChanged();
			return true;
		}
		
		return super.onContextItemSelected(item);
	}
	
	@Override
	protected void onStart() {
		super.onStart();		
		
		IntentFilter intentFilter = new IntentFilter();
        //监听消息
        intentFilter.addAction(KFMainService.ACTION_XMPP_MESSAGE_RECEIVED);
        registerReceiver(mXmppreceiver, intentFilter); 
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		mMessageList = DataSupport.where("username = ?", mUsername).find(MessageEntity.class);
		new ImageTask().execute();
	}
	
	@Override
	protected void onStop() {
		super.onStop();

        unregisterReceiver(mXmppreceiver);
	}
	
	private BroadcastReceiver mXmppreceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if(action.equals(KFMainService.ACTION_XMPP_MESSAGE_RECEIVED)) {
            	String body = intent.getStringExtra("body");
            	String from = StringUtils.parseName(intent.getStringExtra("from"));
            	
            	if (body.contains(VoiceUtil.VOICE_TAG)) {
            		body = VoiceUtil.saveVoice(body);
            	} else if (body.contains(VideoUtil.VIDEO_TAG)) {
            		body = VideoUtil.saveVideo(body);
            	} else if (body.contains(ImageUtil.IMAGE_TAG)) {
            		body = ImageUtil.saveImage(body);
            	}
            	
            	MessageEntity entity = new MessageEntity();
            	entity.setUsername(from);
            	entity.setContent(body);
            	entity.setDate(KFUtils.getDate());
            	entity.setType(0);
            	entity.save(); //存储到数据库
            
            	mMessageList.add(entity);
            	
            	mChatAdapter.notifyDataSetChanged();
            }
        }
    };
    
    private class ImageTask extends AsyncTask<Void, Void, Void> {
    	
    	@Override
    	protected Void doInBackground(Void... arg0) {
    		String sendPath = XmppVCard.getOthersAvatarPath(KFSettingsManager.getSettingsManager(ChatActivity.this).getUsername());
    		String receivePath = XmppVCard.getOthersAvatarPath(mUsername);
    		mSendBitmap = KFImageUtils.getBitmapByPath(sendPath);
    		if (mSendBitmap == null) {
    			mSendBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.head);
    		}
    		mReceiveBitmap = KFImageUtils.getBitmapByPath(receivePath);
    		if (mReceiveBitmap == null) {
    			mReceiveBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.head);
    		}
    		return null;
    	}
    	
    	@Override
    	protected void onPostExecute(Void result) {
    		mChatAdapter = new ChatAdapter(ChatActivity.this, mMessageList, mSendBitmap, mReceiveBitmap);
    		mListView.setAdapter(mChatAdapter);
    		super.onPostExecute(result);
    	}
    }
}
