package com.weicong.openchat.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ToggleButton;

import com.appkefu.lib.service.KFSettingsManager;
import com.appkefu.lib.utils.KFSettings;
import com.weicong.openchat.R;

public class SettingsActivity extends Activity {

	private ToggleButton mRemind;
	private ToggleButton mVoice;
	private ToggleButton mVibrate;
	
	private KFSettingsManager mSettingsMgr;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_settings);
		
		mSettingsMgr = KFSettingsManager.getSettingsManager(this);
		
		getActionBar().setDisplayHomeAsUpEnabled(true);
		setTitle("����");
		
		mRemind = (ToggleButton) findViewById(R.id.tb_remind);
		mVoice = (ToggleButton) findViewById(R.id.tb_voice);
		mVibrate = (ToggleButton) findViewById(R.id.tb_vibrate);
		
		mRemind.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton arg0, boolean isChecked) {
				mSettingsMgr.saveSetting(KFSettings.NEW_MESSAGE_NOTIFICATION, isChecked);
				
				if (!isChecked) {
					mVoice.setChecked(false);
					mVoice.setEnabled(false);
					mVibrate.setChecked(false);
					mVibrate.setEnabled(false);
				} else {
					mVoice.setChecked(true);
					mVoice.setEnabled(true);
					mVibrate.setChecked(true);
					mVibrate.setEnabled(true);
				}
			}
		});
		
		mVoice.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton arg0, boolean isChecked) {
				mSettingsMgr.saveSetting(KFSettings.NEW_MESSAGE_VOICE, isChecked);
			}
		});
		
		mVibrate.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton arg0, boolean isChecked) {
				mSettingsMgr.saveSetting(KFSettings.NEW_MESSAGE_VIBRATE, isChecked);
			}
		});
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
