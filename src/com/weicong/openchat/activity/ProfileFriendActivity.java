package com.weicong.openchat.activity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.appkefu.lib.interfaces.KFIMInterfaces;
import com.appkefu.lib.service.KFMainService;
import com.appkefu.lib.ui.entity.KFVCardEntity;
import com.appkefu.lib.utils.KFImageUtils;
import com.weicong.openchat.R;
import com.weicong.openchat.fragment.RosterListFragment;
import com.ypy.eventbus.EventBus;

public class ProfileFriendActivity extends Activity {

	public static final String USERNAME = "username";
	
	private String mUsername; //用户名
	private String mPath; //头像的路径
	private ImageView mHeadView;
	
	private KFVCardEntity mVcardEntity;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		setTitle("详细资料");
		setContentView(R.layout.activity_profile_friend);
		
		mUsername = getIntent().getStringExtra(USERNAME);
		mVcardEntity = KFIMInterfaces.getVCardOf(mUsername);
		
		TextView usernameText = (TextView) findViewById(R.id.tv_username);
		usernameText.setText(mUsername);
		
		if (mVcardEntity.getNickname() != null) {
			TextView nicknameText = (TextView) findViewById(R.id.tv_nickname);
			nicknameText.setText(mVcardEntity.getNickname());
			findViewById(R.id.nickname).setVisibility(View.VISIBLE);
		}
		
		TextView signatureText = (TextView) findViewById(R.id.tv_sign);
		signatureText.setText(mVcardEntity.getSignature());
		
		mHeadView = (ImageView) findViewById(R.id.iv_image);
		mHeadView.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(ProfileFriendActivity.this, ShowImageActivity.class);
				intent.putExtra(ShowImageActivity.PATH, mPath);
				startActivity(intent);	
			}
		});
		
		Button cancelButton = (Button) findViewById(R.id.btn_cancel);
		cancelButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				//取消好友
				KFIMInterfaces.removeFriend(ProfileFriendActivity.this, mUsername);
				EventBus.getDefault().post(new RosterListFragment.Event(mUsername));
				finish();
			}
		});
		
		Button sendButton = (Button) findViewById(R.id.btn_send);
		sendButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(ProfileFriendActivity.this, ChatActivity.class);
				intent.putExtra(ChatActivity.USERNAME, mUsername);
				startActivity(intent);
				finish();
			}
		});
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		
		IntentFilter intentFilter = new IntentFilter();
        //获取好友头像结果
        intentFilter.addAction(KFMainService.ACTION_IM_GET_FRIEND_AVATAR_RESULT);
        
        registerReceiver(mXmppreceiver, intentFilter);        
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		
		//获取好友头像本地地址
		KFIMInterfaces.getFriendHeadImage(this, mUsername);

	}
	
	@Override
	protected void onStop() {
		super.onStop();
        unregisterReceiver(mXmppreceiver);
	}
	
	private BroadcastReceiver mXmppreceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if(action.equals(KFMainService.ACTION_IM_GET_FRIEND_AVATAR_RESULT)) {
            	mPath = intent.getStringExtra("path");
        		
        		Bitmap bitmap = KFImageUtils.loadImgThumbnail(mPath, 100, 100);
        		if(bitmap != null) {
        			mHeadView.setImageBitmap(bitmap);
        		} else {
        			mHeadView.setImageResource(R.drawable.head);
        		}
            }
        }
    };
    
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
