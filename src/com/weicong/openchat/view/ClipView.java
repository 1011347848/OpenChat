package com.weicong.openchat.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

public class ClipView extends View
{
	private int mSpaceX;
	private int mSpaceY;
	
	private int mUnitLength = 160; // ��λ����
	
	public ClipView(Context context) 
	{
		super(context);
	}

	public ClipView(Context context, AttributeSet attrs)
	{
		super(context, attrs);
	}

	public ClipView(Context context, AttributeSet attrs, int defStyle)
	{
		super(context, attrs, defStyle);
	}

	public int getSpaceX() {
		return mSpaceX;
	}
	
	public int getSpaceY() {
		return mSpaceY;
	}
	
	public int getBoundWidth() {
		return 4 * mUnitLength;
	}
	
	public int getBoundHeight() {
		return 4 * mUnitLength;
	}
	
	@SuppressLint("DrawAllocation")
	@Override
	protected void onDraw(Canvas canvas)
	{
		super.onDraw(canvas);
		int width = this.getWidth();
		int height = this.getHeight();
		
		mSpaceX = (width - 4 * mUnitLength) / 2;
		mSpaceY = (height - 4 * mUnitLength) /2;
		
		Paint paint = new Paint();
		paint.setColor(0xaa000000);
		
		canvas.drawRect(0, 0, width, mSpaceY, paint);
		canvas.drawRect(0, mSpaceY, mSpaceX, height - mSpaceY, paint);
		canvas.drawRect(width - mSpaceX, mSpaceY, width, height - mSpaceY, paint);
		canvas.drawRect(0, height - mSpaceY, width, height, paint);
		
		paint.setColor(0xFFFFFF);
		canvas.drawRect(mSpaceX - 1, mSpaceY - 1, width-mSpaceX + 1, mSpaceY, paint);
		canvas.drawRect(mSpaceX - 1, mSpaceY, mSpaceX, height-mSpaceY, paint);
		canvas.drawRect(width-mSpaceX, mSpaceY, width-mSpaceX + 1, height - mSpaceY, paint);
		canvas.drawRect(mSpaceX - 1, height - mSpaceY, width - mSpaceX + 1, height -mSpaceY + 1, paint);
	}

}
